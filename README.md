docker-kibana
===

Just a simple Kibana instance installed via a Debian package
in a Docker container. Configuration is external and must be
supplied by mounting /etc/kibana.

