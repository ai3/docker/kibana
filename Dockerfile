FROM docker.io/library/debian:bookworm-slim

COPY elastic.gpg /etc/apt/trusted.gpg.d/
COPY build.sh /tmp/build.sh

RUN /tmp/build.sh && rm /tmp/build.sh

ENTRYPOINT ["/usr/share/kibana/bin/kibana", "serve", "--config", "/etc/kibana/kibana.yml", "--quiet"]

