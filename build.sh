#!/bin/sh
#
# Install script for chaperone inside a Docker container.
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES="apt-transport-https ca-certificates"

# Packages required to serve the website and run the services.
# We have to keep the python3 packages around in order to run
# chaperone (installed via pip).
PACKAGES="default-jre-headless kibana-oss"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

die() {
    echo "ERROR: $*" >&2
    exit 1
}

set -x

apt update

# update-alternatives workaround, otherwise fails with e.g.
# update-alternatives: using /usr/lib/jvm/java-11-openjdk-amd64/bin/rmid to provide /usr/bin/rmid (rmid) in auto mode
# https://github.com/debuerreotype/debuerreotype/issues/10
for i in $(seq 1 8); do mkdir -p "/usr/share/man/man${i}"; done


# Add the elastic.co Debian package repository.
# Key at https://artifacts.elastic.co/GPG-KEY-elasticsearch
install_packages ${BUILD_PACKAGES} \
    || die "could not install build packages"
echo "deb https://artifacts.elastic.co/packages/oss-7.x/apt stable main" \
    > /etc/apt/sources.list.d/elastic.list
apt-get -q update

# Install required packages
install_packages ${PACKAGES} \
    || die "could not install packages"

# This is nasty but it's necessary to support 'docker run --user':
# we are not going to know which uid the process will run as, and
# we won't have permissions to chown at the ENTRYPOINT...
chown -R a+rwX \
    /usr/share/kibana/plugins \
    /usr/share/kibana/optimize \
    /var/lib/kibana

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
